﻿# 你好，Springcloud，初次见面，请多多指教

school-springcloud-springboot是一个基于Springcloud的课程管理系统，它是什么系统其实不是很重要，关键是它利用Springboot的开发便利性巧妙地简化了分布式系统基础设施的开发，加入了Springcloud系列框架特有服务发现注册、配置中心、消息总线、负载均衡、断路器、数据监控等功能，可以让你真正的感受到**微服务**的魅力。所以Springcloud几乎是现在微服务开源界最好的产品了。与Dubbo比较起来，Springcloud的使用会让你更加的爱不释手。

## 一、运行工具、技术与环境

* 运行环境：JDK 11，gradle 5+
* 技术栈：SpringBoot2、Druid、Thymeleaf、Mybatis
* **微服务Springcloud技术栈：Spring Cloud Config、Spring Cloud Bus、Eureka、Hystrix、Spring Cloud Gateway、Spring Cloud Sleuth、Ribbon、Feign、zipkin、hystrix-dashboard**
* 工具：IntelliJ IDEA、谷歌浏览器、Mysql、RabbitMq

## 二、Springboot快速集成Springcloud关键的依赖
```gradle
dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.1.3.RELEASE")
}

dependencyManagement {
    imports {
        mavenBom "org.springframework.cloud:spring-cloud-dependencies:Greenwich.SR1"
    }
}

```
## 三、各个module介绍
1、eureka-server：基于Eureka的注册中心，提供服务发现与注册。

2、gateway-server：基于spring自家的Spring Cloud Gateway微服务网关，它是底层是webflux，也可以提供动态路由与负载分配等等。

3、config-server：配置中心服务端。

4、hystrix-dashboard：基于Hystrix的仪表盘组件的module，可以通过网页实时浏览Hystrix的各项指标信息。

5、main-operation：提供CourseType的服务的项目，里面包括配置中心、feign等技术。

6、user-operation：提供User的服务的项目，包括Spring Cloud Sleuth、feign等技术。

7、zipkin-server：Zipkin分布式跟踪系统。

8、main-school：主操作系统。

## 四、使用步骤
1.将项目导入IntelliJ IDEA，gradle加载jar包。

2.将doc文件夹里面的sql脚本导入到mysql中，使得数据库里面有数据。

3、修改main-operation和user-operation数据库配置。

3.项目启动顺序（这样启动比较合适，当然熟悉之后自行更改启动顺序，**还有，还有：计算机内存至少8g**）：

（1）eureka-server

（2）gateway-server

（3）config-server

（4）zipkin-server

（5）main-operation

（6）user-operation

（7）main-school

（8）hystrix-dashboard


4.打开浏览器，输入网址[http://localhost:8095](http://localhost:8095)即可浏览（账号：000101  密码：123456）。

**注意：** 若是该项目对初学者有困难的话，请先学习school-springcloud-springboot的简化版项目，地址如下：
* 码云：[https://gitee.com/smirk/easy-springcloud](https://gitee.com/smirk/easy-springcloud)
* github：[https://github.com/xiaoze-smirk/school-springcloud-springboot](https://github.com/xiaoze-smirk/school-springcloud-springboot)

## 五、总结
您要是喜欢，请拿去。您的受益，是我最大的动力。

------

smirk小泽   
2018 年 07月17日    
